package com.kaaryaka.notificationServer.firebase;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

import java.io.FileInputStream;
import java.io.IOException;
import javax.annotation.PostConstruct;

import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Value;

@Service
@Slf4j
public class FCMInitializer {
//	@Value("${GOOGLE_APPLICATION_CREDENTIALS}")
//    private String gacFilePath;
	
	@PostConstruct
	public void Initialize() {
		try {
			//log.info(gacFilePath);
			FileInputStream serviceAccount =
					  new FileInputStream("E:\\Kaaryaka\\kaaryaka-c91d5-firebase-adminsdk-lyxww-8350d8f7e5.json");

			FirebaseOptions options = new FirebaseOptions.Builder()
			  .setCredentials(GoogleCredentials.fromStream(serviceAccount))
			  .setDatabaseUrl("https://kaaryaka-c91d5.firebaseio.com")
			  .build();

			FirebaseApp.initializeApp(options);
			
		} catch (IOException e) {
			log.error(e.getMessage());
		}
	}
}
