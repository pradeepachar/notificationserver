package com.kaaryaka.notificationServer.firebase;

import java.util.concurrent.ExecutionException;
import java.time.Duration;

import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;

import com.google.firebase.messaging.*;
import com.kaaryaka.notificationServer.NotificationRequest;

@Service
@Slf4j
public class FCMService {
	public void sendMessage( NotificationRequest request, String registrationToken )
			throws InterruptedException, ExecutionException {
		Message message = Message.builder()
				.putData("AppName", "Kaaryaka Trade")
				.putData("message", "welcome")
				.setToken(registrationToken)
				.build();
		
		try {
			String response;
			response = FirebaseMessaging.getInstance().send(message);
			log.info(response);
		} catch (FirebaseMessagingException e) {
			log.error(e.getMessage());
		}
		
	}
}
