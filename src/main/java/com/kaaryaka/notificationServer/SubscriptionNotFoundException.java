package com.kaaryaka.notificationServer;

class SubscriptionNotFoundException extends RuntimeException {

	SubscriptionNotFoundException(Long id) {
    super("Could not find subscription " + id);
  }
}