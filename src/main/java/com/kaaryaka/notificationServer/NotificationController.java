/**
 * 
 */
package com.kaaryaka.notificationServer;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author pradeep
 *
 */
@RestController
public class NotificationController {

	private NotificationService notificationService;
	
	NotificationController( SubscriptionRepository repository ) {
		this.notificationService = new NotificationService( repository );
	}
	
	@PostMapping("/notifications")
	public ResponseEntity sendNotification( @RequestBody NotificationRequest request ) {
		notificationService.disptachNotification(request);
	    return new ResponseEntity<>(new NotificationResponse(HttpStatus.OK.value(), request.toString()), HttpStatus.OK);
	}
	
	@GetMapping("/notifications")
	public ResponseEntity all() {
	    return new ResponseEntity<>(new NotificationResponse(HttpStatus.OK.value(), "Notification sent successfully"), HttpStatus.OK);
	  }
}
