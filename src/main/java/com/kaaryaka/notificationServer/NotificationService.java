/**
 * 
 */
package com.kaaryaka.notificationServer;

import org.springframework.stereotype.Service;

import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import java.util.concurrent.ExecutionException;

import com.kaaryaka.notificationServer.firebase.FCMService;

/**
 * @author Pradeep
 * Service handling the notifications
 */
@Service
@Slf4j
public class NotificationService {
	private final SubscriptionRepository repository;
	private FCMService fcmService;
	
	NotificationService ( SubscriptionRepository repository ) {
		this.repository = repository;
		fcmService = new FCMService();
	}

	public void disptachNotification( NotificationRequest request ) {
		Long id = request.getSubscriptionId();
		Subscription subscription = repository.findById(id)
			    .orElseThrow(() -> new SubscriptionNotFoundException(id));
		
		if ( subscription != null ) {
			try {
			if (fcmService == null )
				log.error( "FCM service is null");
				fcmService.sendMessage(request, subscription.getToken());
			} catch (InterruptedException | ExecutionException e) {
				log.error(e.getMessage());
			}
			log.info(request.getUserId() + " found");
		}
	}
}
