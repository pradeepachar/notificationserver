package com.kaaryaka.notificationServer;

import lombok.extern.slf4j.Slf4j;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
class LoadDatabase {

  @Bean
  CommandLineRunner initDatabase(SubscriptionRepository repository) {
    return args -> {
      log.info("Preloading " + repository.save(new Subscription("Pradeep", "Instance1","token1")));
      log.info("Preloading " + repository.save(new Subscription("Achar", "Instance2", "token2")));
    };
  }
}