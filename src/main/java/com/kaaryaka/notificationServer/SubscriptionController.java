/**
 * 
 */
package com.kaaryaka.notificationServer;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

/**
 * @author Pradeep
 * Rest Controller for subscription management
 */
@RestController
public class SubscriptionController {
	private final SubscriptionRepository repository;
	
	SubscriptionController(SubscriptionRepository repository ) {
		this.repository = repository;
	}
	
	@GetMapping("/subscriptions")
	List<Subscription> all() {
	    return repository.findAll();
	  }

	@PostMapping("/subscriptions")
	Subscription newSubscription(@RequestBody Subscription newSubscription) {
			return repository.save(newSubscription);
		}

	@GetMapping("/subscriptions/{id}")
	Subscription one(@PathVariable Long id) {
		 Subscription subscription = repository.findById(id)
	    .orElseThrow(() -> new SubscriptionNotFoundException(id));
		 
		 return subscription;
	}
	
	@DeleteMapping("/subscriptions/{id}")
	void deleteEmployee(@PathVariable Long id) {
	    repository.deleteById(id);
	}
}