/**
 * 
 */
package com.kaaryaka.notificationServer;

import lombok.Data;

/**
 * @author pradeep
 * Object accepting the request message body from the clients
 *
 */
@Data
public class NotificationRequest {
	private Long subscriptionId;
	private String userId;
	private String title;
	private String message;
	
	NotificationRequest() {}
	
	NotificationRequest( Long subscriptionId, String userId, String title, String message ) {
		this.subscriptionId = subscriptionId;
		this.userId = userId;
		this.title = title;
		this.message = message;
	}
}
