/**
 * 
 */
package com.kaaryaka.notificationServer;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
/**
 * @author pradeep
 * This object to maintain the subscription for push notifications 	
 */

@Data
@Entity
public class Subscription {
	private @Id @GeneratedValue Long subscriptionId;
	private String userId;
	private String instanceId;
	private String token;
	
	Subscription() {}

	Subscription(String userId, String instanceId, String token) {
		this.userId = userId;
		this.instanceId = instanceId;
		this.token = token;
	}
}
