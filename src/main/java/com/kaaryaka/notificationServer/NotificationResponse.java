/**
 * 
 */
package com.kaaryaka.notificationServer;

/**
 * @author pradeep
 * The response sent to the client which invoked the notification request
 * 
 */
public class NotificationResponse {
	private int status;
	private String message;
	
	public NotificationResponse() {}
	
	public NotificationResponse( int status, String message ) {
		this.status = status;
		this.message = message;
	}
	
	public int getStatus() {
		return this.status;
	}
	
	public void setStatus( int status ) {
		this.status = status;
	}
	
	public String getMessage() {
		return this.message;
	}
	
	public void setMessage( String message ) {
		this.message = message;
	}
}
